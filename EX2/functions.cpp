#include "functions.h"

template <class TYPE>
TYPE compare(TYPE num1, TYPE num2)
{
	TYPE result;
	if (num1 > num2)
		result = -1;
	else if (num1 < num2)
		result = 1;
	else if (num1 == num2)
		result = 0;
	return result;
}