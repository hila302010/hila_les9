#include <iostream>

template <class TYPE>
int compare(TYPE num1, TYPE num2);

template <class TYPE>
void bubbleSort(TYPE* doubleArr, TYPE size);

template <class TYPE>
void printArray(TYPE* doubleArr,TYPE arr_size);
int main() {

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('b', 'a') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;

	//check bubbleSort
	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}

// compare
template <class TYPE>
int compare(TYPE num1, TYPE num2)
{
	TYPE result;
	if (num1 > num2)
		result = -1;
	else if (num1 < num2)
		result = 1;
	else if (num1 == num2)
		result = 0;
	return result;
}
// bubbleSort
template <class TYPE>
void bubbleSort(TYPE* doubleArr, TYPE size)
{
	for (int j = 0; j < size - 1; j++)
	{
		if (doubleArr[j] > doubleArr[j + 1]) {
			TYPE temp = doubleArr[j];
			doubleArr[j] = doubleArr[j + 1];
			doubleArr[j + 1] = temp;
		}
	}
}
// printArray
template <class TYPE>
void printArray(TYPE* doubleArr, TYPE arr_size)
{
	for (int i = 0; i < arr_size; i++) 
	{
		std::cout << doubleArr[i] << " ";
	}
}