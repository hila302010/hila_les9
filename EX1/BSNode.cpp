#include "BSNode.h"
#include <iostream>
//6 2 # 3 # 5 # # 8 # 9 # #
// empty space - left, # - right from curr, ## right from head
BSNode::BSNode(string data)
{
	_data = data;
	_count = 1;
	_right = NULL;
	_left = NULL;
}

BSNode::BSNode(const BSNode & other)
{
	_data = other._data;
	_left = other._left;
	_right = other._right;
}

BSNode::~BSNode()
{
	if (_right)
		delete _right;
	if (_left)
		delete _left;
}

void BSNode::insert(string value)
{
	if (value > _data)
	{
		if (_right == NULL) 
			_right = new BSNode(value);

		else 
			_right->insert(value);
	}
	else if (value < _data)
	{
		if (_left == NULL) 
			_left = new BSNode(value);
		
		else 
			_left->insert(value);

	}
	else
		_count++;
}

BSNode & BSNode::operator=(const BSNode & other)
{
	_data = other._data;
	_left = other._left;
	_right = other._right;
	return *this;
}

bool BSNode::isLeaf() const
{
	return !(_right || _left);
}

string BSNode::getData() const
{
	return _data;
}

BSNode * BSNode::getLeft() const
{
	return this->_left;
}

BSNode * BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(string val) const
{

	bool ans = false;
	if (val > _data && _right)
	{
		ans = _right->search(val);
	}
	else if (val < _data && _left)
	{
		ans = _left->search(val);
	}
	else if (val == _data)
	{
		ans = true;
	}
	return ans;
}

int BSNode::getHeight() const
{
	int rightHeight = 0;
	int leftHeight = 0;
	if (isLeaf())
	{
		return 0;
	}
	if (_right)
	{
		rightHeight = _right->getHeight() + 1;
	}
	if (_left)
	{
		leftHeight = _left->getHeight() + 1;
	}
	if (rightHeight < leftHeight)
		return leftHeight;
	else
		return rightHeight;
}

int BSNode::getDepth(const BSNode & root) const
{
	if (root.search(_data) == NULL)
	{
		return -1;
	}
	return getCurrNodeDistFromInputNode(&root);
}

void BSNode::printNodes() const
{
	if (_left)
	{
		_left->printNodes();
	}
	std::cout << _data << " " << _count << endl;
	if (_right)
	{
		_right->printNodes();
	}
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode * node) const
{
	if (_data == node->_data)
	{
		return 0;
	}
	if (_data > node->_data) // right
	{
		return getCurrNodeDistFromInputNode(node->_right) + 1;
	}
	if (_data < node->_data) // left
	{
		return getCurrNodeDistFromInputNode(node->_left) + 1;
	}
	return -1;
}
