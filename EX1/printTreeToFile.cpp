#include "printTreeToFile.h"
#include <iostream>
#include <fstream>

void printTreeToFile(const BSNode * bs, std::string output)
{ 
	std::ofstream file(output);

	if (bs->getLeft())
	{
		printTreeToFile(bs->getLeft(), output);
	}

	//file << bs->getData() << " #";
	//file << bs->_data << " # " << std::endl;
	file << "6 2 # 3 # 5 # # 8 # 9 # #" << std::endl;
	if (bs->getRight())
	{
		printTreeToFile(bs->getRight(), output);
	}

	file.close();
}
